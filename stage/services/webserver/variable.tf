variable "port_number" {
    description = "Port number of the web server"
    type = number
}