provider "aws" {
    region = "us-east-2"
}

data "aws_vpc" "default" {
    default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}

resource "aws_security_group" "webserver_alb_sg" {
    name = "webserver-alb-sg"
    ingress {
       from_port   = var.port_number
       to_port     = var.port_number
       protocol    = "tcp"
       cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
       from_port   = 0
       to_port     = 0
       protocol    = "-1"
       cidr_blocks = ["0.0.0.0/0"]
    }
}



resource "aws_lb" "webserver_lb" {
  name               = "test-lb-tf"
  internal           = false
  security_groups    = [aws_security_group.webserver_alb_sg.id]
  load_balancer_type = "application"
  subnets            = data.aws_subnet_ids.default.ids
  
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.webserver_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code  = "404"
    }
  }
}

resource "aws_lb_listener_rule" "asg" {
  listener_arn  = aws_lb_listener.http.arn
  priority  = 100

  condition {
    path_pattern {
    values = ["*"]
    }
  }
    action {
      type = "forward"
      target_group_arn = aws_lb_target_group.asg.arn
    }
  }


resource "aws_lb_target_group" "asg" {
  name_prefix     = "tf"
  port     = var.port_number
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.default.id

  lifecycle {
        create_before_destroy = true
    }  
  
  health_check {
      path                  = "/"
      protocol              = "HTTP"
      matcher               = "200"
      interval              = 15
      timeout               = 3
      healthy_threshold     = 2
      unhealthy_threshold   = 2
  }
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}
resource "aws_launch_configuration" "test" {
    image_id = "ami-08962a4068733a2b6"                           #"ami-089c6f2e3866f0f14"
    instance_type = "t2.micro"
    
    #associate_public_ip_address = true
    security_groups = [aws_security_group.instance.id]
    user_data = <<-EOF
    #!/bin/bash
    echo "Hello, World" > index.html
    nohup busybox httpd -f -p ${var.port_number} &
    EOF

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_security_group" "instance" {
    name = "webserver-sg"
    ingress {
       from_port   = var.port_number
       to_port     = var.port_number
       protocol    = "tcp"
       cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_autoscaling_group" "asg-webserser-dev" {
    name_prefix             = "asg_webserver_dev"
    desired_capacity        = 2
    launch_configuration    = aws_launch_configuration.test.id
    target_group_arns       = [aws_lb_target_group.asg.arn]
    health_check_type       = "ELB"
    min_size                = 2
    max_size                = 6
    vpc_zone_identifier     = data.aws_subnet_ids.default.ids  

    
    tag {
        key                 = "Name"
        value               = "teraform-asg-example"
        propagate_at_launch = true

    }
    
}