output "public_ip" {
    value = aws_lb.webserver_lb.dns_name
    description = "Public IP of the webserver instance"
}