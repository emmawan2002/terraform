--backend process--
  - s3 bucket and dynamodb has to be created first
  - terraform block has to be use to create backend 
    with dynamodb_table attribute use to specify the table name.
    encryption attribute can also be used to specify if we want terraform
    state file to be encrypted
  - if you want to delet the s3 bucket and dynamodb table
    - go to the terraofrm code and remove the backend configuration and rerun terraform init to copy the terraform state back to your local disk
    - run terraofrm destory to delete the s3 bucket and dynamodb table
  - Backend block does not allow the use of variables and references
    - Partial configuration;
      - You could extract repeated backend configuration and pass it via command line
        using -backend-config argement when calling the terraform init command
        # backend.hcl
        bucket    = bucketname
        region    = us-east-2
        dynamodb_table = dynameble_table_name
        encrypt   = true
      - only the key parameter remains in the terraform code
        terraform {
            backend "s3" {
                key = "example/terraform.tfstate"
            }
        }
      - to put the partical configuration together
        terraform init -backend-config=backend.hcl
      - Alternative is to use terragrunt tool

--Two ways to isolate your terraform state file--
  - Isolation via workspace
    The default workspsace will use the key specified in the terraform object
    New workspaces will be created within the env:/example1/workspace-example/terraform.tfstate 
    Second workspace env:/example2/workspace-example/terraform.tfstate
    this is good when you already have some modules deployed and you want to experiment with it
    but you don't want your experiment to affect the terraform state file
    example code
      instance_type = terraform.workspace == "default"? "t2.micro":"t2.medium"
    coommands  
      terraform workspace show (show the current workspace)
      terraform workspace new example1 (create a new workspace)
      terraform workspace list (list workspaces)
      terraform workspace select workspace_name (select workspace you want to work in)
      this
    Drawbacks
        - all state files are stored in same backend i.e same s3 bucket
          i.e same access and authentication mechanism which is not suitable for prod env
        - workspaces are not visible in the code or on the terminal unless your run trrform workspace commands. When browsing code , a modle that has been deplyed in one workspace looks exactly the same as a module deployed in 10 workspaces.
        - worksapces are error prone. The lack of visibility makes it easy to forget what workspace your are in and accidentally make changes in the wrong one.

  - Isolation via file layout
    - Put the configuration files for each envionment into a separate folder e.g staging or productio folders
    - Configure a different backend for each each enviroment using different authentication mechanism  and access controls (each enviroment could live in a separate AWS account with a separate S3 bucket as backend)
    gen
    - create folder structure per component
      within each folder there are below configuration files
      - variables.tf
        input variables
      - outputs.tf
        Output variables
      - main.tf
        - The resources
        - if the main file is becoming extremely large, you could break it up into s3.tf, ec2.tf. 
        - it is suitable to instead use modules once it gets to that size

      



    