provider "aws" {
    region = "us-east-2"
}

resource "aws_instance" "workspace_instance" {
    ami = "ami-08962a4068733a2b6"                          
    instance_type = terraform.workspace == "default"? "t2.micro":"t2.medium"
}