terraform {
  backend "s3" {
    bucket = "terraform-remote-state-dev0328"
    key    = "workspace-example/terraform.tfstate"
    region = "us-east-2"

    dynamodb_table = "terraform-remote-state-lock-dev"
    encrypt = true
  }
}